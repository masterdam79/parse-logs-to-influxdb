#!/usr/bin/env python3

import configparser
from influxdb import InfluxDBClient
import argparse
import json
from datetime import datetime
import socket
import os


parser = argparse.ArgumentParser()
parser = argparse.ArgumentParser(description='Process some arguments.')
parser.add_argument('--date', type=int, required=False)
parser.add_argument('--hour', type=int, required=False)
args = parser.parse_args()

# Get some variables outside this script
config = configparser.ConfigParser()

try:
    f = open("config.cfg", 'rb')
except OSError:
    print("Could not open/read file: config.cfg")
    sys.exit()


with f:
    config.read("config.cfg")
    influx_host = config['DETAILS']['INFLUXDB_URL']
    db_name = config['DETAILS']['DB_NAME']
    measurement = config['DETAILS']['MEASUREMENT']

if args.date:
    formatted_date = args.date
else:
    formatted_date = datetime.now().strftime('%Y%m%d')

host_name = socket.gethostname()
home_dir = os.environ['HOME']

log_file = home_dir + "/LOG/" + formatted_date + "-" + host_name + ".log"


print(log_file)

client = InfluxDBClient(host=influx_host, port=8086)
client.create_database(db_name)
client.get_list_database()
client.switch_database(db_name)

# open file in read mode
with open(log_file, 'r', encoding='latin-1') as read_obj:
    # Iterate over each row in the csv using reader object
    for row in read_obj.readlines():
        # row variable is a list that represents a row in csv
        #print(row)
        some_vars = row.split('\t')
        datetime = some_vars[0].split('T', 1)
        date = datetime[0]
        time = datetime[1]
        print(time)
        #tz_now = datetime.now(tz)
        #offset = tz_now.utcoffset()
        #time_now = datetime.time(tz_now)
        #date_now = datetime.date(tz_now)
        #datetime_formatted = str(date_now) + " " + str(time_now.hour) + ":" + str(time_now.minute) + ":00.000000+" + str(offset)
        formattedtime = datetime.strftime("%Y-%m-%d %H:%M:00.000000%z")
        print(formattedtime)
        exit()

        try:
            some_vars[1]
            application_host_title = some_vars[1].split('  ', 1)
            application = application_host_title[0]
            host_title = application_host_title[1].split(' ', 1)
            host_name_logfile = host_title[0]
            title = host_title[1].lstrip()

            print("Date & Time: " + datetime + ", Application: " + application + ", Title: " + title)
        except:
            print("nah")


print(log_file)
