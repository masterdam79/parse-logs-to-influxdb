# parse-logs-to-influxdb

A Python script that can be run as a crontab to take the logs of the last hour if not specified and place it in InfluxDB, mimicking central logging to some extent.

# Setup

1. Install ```influxdb``` using ```pip```
  - ``python3 -m pip install influxdb```
2. Rename ```config``` to ```config.cfg```
  - Adapt values in ```config.cfg``` to match your environment
